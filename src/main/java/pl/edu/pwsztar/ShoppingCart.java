package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart implements ShoppingCartOperation {

    List<CartProduct> allProducts = new ArrayList<>();

    public boolean addProducts(String productName, int price, int quantity) {

        if (allProducts.size() == 0 && !productName.equals("") && price > 0 && quantity > 0) {
            CartProduct cartProduct = new CartProduct(productName, price, quantity);
            allProducts.add(cartProduct);
            return true;
        } else if (allProducts.size() == ShoppingCart.PRODUCTS_LIMIT
                || productName.equals("") || price <= 0 || quantity <= 0) {
            return false;
        }

        for (CartProduct product : allProducts) {
            if (product.getProductName().equals(productName) && product.getPrice() != price) {
                return false;
            }
            CartProduct cartProduct = new CartProduct(productName, price, quantity);
            allProducts.add(cartProduct);
            return true;
        }
        return false;
    }

    public boolean deleteProducts(String productName, int quantity) {
        for (CartProduct product : allProducts) {
            if (product.getProductName().equals(productName) && product.getQuantity() >= quantity) {
                if (product.getQuantity() == quantity) {
                    allProducts.remove(product);
                    return true;
                }
                product.setQuantity(product.getQuantity() - quantity);
                return true;
            }
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        int quantity = 0;
        for (CartProduct product : allProducts) {
            if (product.getProductName().equals(productName)) {
                quantity += product.getQuantity();
            }
        }
        return quantity;
    }

    public int getSumProductsPrices() {
        int sum = 0;
        for (CartProduct product : allProducts) {
            sum += product.getPrice();
        }
        return sum;
    }

    public int getProductPrice(String productName) {
        for (CartProduct product : allProducts) {
            if (product.getProductName().equals(productName)) {
                return product.getPrice();
            }
        }
        return 0;
    }

    public List<String> getProductsNames() {
        ArrayList<String> productsNames = new ArrayList<>();
        for (CartProduct product : allProducts) {
            productsNames.add(product.getProductName());
        }
        return new ArrayList<>(productsNames);
    }
}
